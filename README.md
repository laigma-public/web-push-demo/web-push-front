<img src="./src/assets/lp-dark.png" alt="Ligma Prods" height="60"/>
<img src="./src/assets/vue-logo.png" alt="VueJS" height="60"/>

# Web Push Demo (VueJS SPA)

## Requisitos
- node 16.x o superior
- npm

## Instrucciones

### 1- Instalar dependencias
```
npm install
```

### 2- Crear .env en carpeta raiz
```
VITE_BASE_PATH=/web-push-demo
```
   
### 4- Ejecutar localmente
#### Servidor caliente
```
npm run dev
```
#### Compila y sirve (para usar ServiceWorker en localhost)
```
npm run preview
```

----

## Enlaces de interés
- https://expressjs.com/es/
- https://www.typescriptlang.org/

____

## Pasos a seguir

1. Instalar axios y conectar API de web-push de backend

2. Modificar service worker para actuar cuando recibe notificaciones push
