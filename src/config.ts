import packageJson from '../package.json'

export default {
  publicVapidKey: import.meta.env.VITE_PUBLIC_VAPID_KEY,
  awsApiUrl: import.meta.env.VITE_AWS_API_URL,
  aesKey: import.meta.env.VITE_AES_KEY,
  appName: packageJson.name,
  appVersion: packageJson.version,
}
