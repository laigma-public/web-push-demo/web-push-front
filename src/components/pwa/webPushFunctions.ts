import config from '@/config';
import utils from '@/global/utils';

export default {
  async checkSubscription() {
    const registration = await navigator.serviceWorker.ready;
    if (!registration) return null;
    const subscription = await registration.pushManager.getSubscription();
    if (!subscription) return null;
    return subscription;
  },

  checkPermission() {
    if (!('Notification' in window)) return 'denied';
    return Notification.permission;
  },

  async showSubscriptionBanner() {
    if (!('serviceWorker' in navigator)) return false;
    if (this.checkPermission() === 'denied') return false;
    if (await this.checkSubscription()) return false;
    return true;
  },

  async subscribe() {
    const registration = await navigator.serviceWorker.ready;
    if (!registration) return null;
    const subscription = await registration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: utils.urlBase64ToUint8Array(config.publicVapidKey),
    });
    return subscription;
  },

  async unsubscribe() {
    const registration = await navigator.serviceWorker.ready;
    const subscription = await registration.pushManager.getSubscription();
    if (!subscription) return;
    await subscription.unsubscribe();
  },
}
