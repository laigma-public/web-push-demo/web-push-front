import CryptoJS from "crypto-js";
import config from "@/config"

export default {
  encrypt(src: string) {
    return CryptoJS.AES.encrypt(src, config.aesKey).toString()
  },

  decrypt(src: string) {
    const bytes = CryptoJS.AES.decrypt(src, config.aesKey)
    const originalText = bytes.toString(CryptoJS.enc.Utf8)
    return originalText
  }
}
