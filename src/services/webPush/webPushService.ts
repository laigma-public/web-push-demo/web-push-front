import api from '@/services/webPush/api';
import cryptoService from '@/services/cryptoService';
import config from '@/config';
import { v4 as uuidv4 } from 'uuid';

export default {
  async subscribe(subscription: any) {
    const parsedSubscription = {
      ID: uuidv4(),
      Fecha_suscripcion: new Date().toISOString(),
      application: config.appName,
      user: 'test',
      subscriptionObject: subscription
    };
    const encryptedSubscription = cryptoService.encrypt(JSON.stringify(parsedSubscription));
    return api().post('/webPush/subscribe', { body: encryptedSubscription });
  },


  async unsubscribe(subscription: any) {
    const encryptedSubscription = cryptoService.encrypt(JSON.stringify(subscription));
    return api().post('/webPush/unsubscribe', { body: encryptedSubscription });
  }
};
