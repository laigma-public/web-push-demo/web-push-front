import { precacheAndRoute } from 'workbox-precaching';
import ligmaLogo from './assets/lp-dark.png';

precacheAndRoute(self.__WB_MANIFEST);

self.addEventListener('push', (event) => {
  const data = event.data.json();
  const options = {
    body: data.body,
    icon: ligmaLogo,
    requireInteraction: true,
    actions: [
      { action: 'open-app', title: 'Abrir la App' }
    ]
  };

  self.registration.showNotification(data.title, options);
});

self.addEventListener('notificationclick', (event) => {
  event.notification.close();

  if (event.action === 'open-app') {
    event.waitUntil(
      clients.openWindow('/web-push-demo')
    );
  }
});